﻿/*
 * Arguments
 *
 * Drills, Welders, Grinders, Tools
 * -on: Turn on
 * -off: Turn off
 * -toggle: Toggle on/off
 * 
 * Batteries
 * -recharge
 * -discharge
 * -auto
 * 
 * Dock: Try to dock all connectors of the vehicle.
 * -recharge: Set all vehicle batteries to recharge if docking was succesful.
 * -standby: Turn off all vehicle thrusters and gyroscopes if docking was succesful.
 * 
 * Undock: Undock all connectors on vehicle.
 * -discharge: Set all batteries to discharge.
 * -auto: Set all batteries to auto.
 * -startup: All engines on, gyroscopes on.
 * 
 * Thrusters
 * -[forward,backward,left,right,up,down]: Select direction of selected thrusters.
 * If no direction selected, selects all thrusters.
 * 
 * -on: All selected thrusters on.
 * -off: All selected thrusters off.
 * -toggle: Toggle all selected thrusters.
 * 
 * -full: All thrusters to full override.
 * -stop: Disable all thruster overrides.
 * -overridetoggle: Toggle all thrusters override.
 * 
 * -set: Sets the override percentage for selected thrusters.
 * -increase: Increments the override percentage for selected thrusters by percentage point determined as number after this switch.
 * -decrease: Decrements the override percentage for selected thrusters by percentage point determined as number after this switch.
 *  
 * Lights
 * -on
 * -off
 * -setupnavlights: Set up navigation lights.
 * Lights that have "nav" in their name.
 * Colors according to light name containing the following:
 * left: Red.
 * right: Green.
 * back: White.
 * 
 * Colors of lights that have "strobe" in their name:
 * If name contains "wing": White.
 * Otherwise: Red.
 * 
 */