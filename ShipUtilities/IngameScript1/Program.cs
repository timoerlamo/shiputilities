﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
	partial class Program : MyGridProgram
	{
		private string GroupSwitch = "group";

		public Program()
		{
			_commands["drills"] = DrillsCommand;
			_commands["welders"] = WeldersCommand;
			_commands["grinders"] = GrindersCommand;
			_commands["tools"] = ToolsCommand;

			_commands["batteries"] = BatteriesCommand;

			_commands["dock"] = DockCommand;
			_commands["undock"] = UndockCommand;
			_commands["thrusters"] = ThrustersCommand;

			_commands["lights"] = LightsCommand;
		}

		#region Main program stuff

		#region Command line argument handling
		private MyCommandLine _commandLine = new MyCommandLine();
		private Dictionary<string, Action> _commands = new Dictionary<string, Action>(StringComparer.OrdinalIgnoreCase);
		#endregion

		public void Main(string argument, UpdateType updateSource)
		{
			if (_commandLine.TryParse(argument))
			{
				Action commandAction;
				for (int i = 0; i < _commandLine.ArgumentCount; i++)
				{
					var command = _commandLine.Argument(i).ToLower();

					if (command == null)
					{
						Error("No command specified");
					}
					else if (_commands.TryGetValue(command, out commandAction))
					{
						// We have found a command. Invoke it.
						commandAction();
					}
					else
					{
						Error($"Unknown command {command}");
					}
				}
			}
			else
			{
				Error("No arguments given.");
			}
		}

		private string CurrentTime
		{
			get { return DateTime.Now.TimeOfDay.ToString(); }
		}

		public void Error(string msg)
		{
			Echo(CurrentTime + " -- " + "Error: " + msg);
		}

		public void Save()
		{
		}
		#endregion

		#region Command line delegates
		private void DrillsCommand()
		{
			if (_commandLine.Switch("on")) { Drills.ForEach(EnableOn); }
			else if (_commandLine.Switch("off")) { Drills.ForEach(EnableOff); }
			else if (_commandLine.Switch("toggle")) { Drills.ForEach(ToggleEnable); }
		}

		private void WeldersCommand()
		{
			if (_commandLine.Switch("on")) { Welders.ForEach(EnableOn); }
			else if (_commandLine.Switch("off")) { Welders.ForEach(EnableOff); }
			else if (_commandLine.Switch("toggle")) { Welders.ForEach(ToggleEnable); }
		}

		private void GrindersCommand()
		{
			if (_commandLine.Switch("on")) { Grinders.ForEach(EnableOn); }
			else if (_commandLine.Switch("off")) { Grinders.ForEach(EnableOff); }
			else if (_commandLine.Switch("toggle")) { Grinders.ForEach(ToggleEnable); }
		}

		private void ToolsCommand()
		{
			if (_commandLine.Switch("on"))
			{
				Drills.ForEach(EnableOn);
				Grinders.ForEach(EnableOn);
				Welders.ForEach(EnableOn);
			}
			else if (_commandLine.Switch("off"))
			{
				Drills.ForEach(EnableOff);
				Grinders.ForEach(EnableOff);
				Welders.ForEach(EnableOff);
			}
			else if (_commandLine.Switch("toggle"))
			{
				Drills.ForEach(ToggleEnable);
				Grinders.ForEach(ToggleEnable);
				Welders.ForEach(ToggleEnable);
			}
		}

		private void BatteriesCommand()
		{
			if (_commandLine.Switch("recharge")) { Batteries.ForEach(BatteryRecharge); }
			else if (_commandLine.Switch("discharge")) { Batteries.ForEach(BatteryDischarge); }
			else if (_commandLine.Switch("auto")) { Batteries.ForEach(BatteryAuto); }
			else
			{
				Error("Can't set battery state, no state specified. Use -recharge, -discharge or -auto flag with command.");
			}
		}

		private void DockCommand()
		{
			Connectors.ForEach(ConnectorConnect);

			if (_commandLine.Switch("recharge")) { if (isDocked) { Batteries.ForEach(BatteryRecharge); } }

			if (_commandLine.Switch("standby")) { if (isDocked) { Thrusters.ForEach(EnableOff); Gyroscopes.ForEach(EnableOff); } }
		}

		private void UndockCommand()
		{
			if (_commandLine.Switch("discharge")) { Batteries.ForEach(BatteryDischarge); }
			else if (_commandLine.Switch("auto")) { Batteries.ForEach(BatteryAuto); }

			if (_commandLine.Switch("startup")) { Thrusters.ForEach(EnableOn); Gyroscopes.ForEach(EnableOn); }

			Connectors.ForEach(ConnectorDisconnect);
		}

		private void ThrustersCommand()
		{
			if (_commandLine.Switch("on")) { Thrusters.ForEach(EnableOn); }
			else if (_commandLine.Switch("off")) { Thrusters.ForEach(EnableOff); }
			else if (_commandLine.Switch("toggle")) { Thrusters.ForEach(ToggleEnable); }

			if (_commandLine.Switch("full")) { Thrusters.ForEach(ThrusterOverrideFull); }
			else if (_commandLine.Switch("stop")) { Thrusters.ForEach(ThrusterOverrideNone); }
			else if (_commandLine.Switch("overridetoggle")) { Thrusters.ForEach(ThrusterOverrideToggle); }
			else if (_commandLine.Switch("set"))
			{
				try
				{
					Thrusters.ForEach(Thruster => ThrusterOverrideSet(Thruster, float.Parse(_commandLine.Switch("set", 0))));
				}
				catch
				{
					Echo("Unable to parse thruster override set command's switch argument.\nArgument must be a decimal number between 0 and 100.\n");
				}
			}
			else if (_commandLine.Switch("increase"))
			{
				try
				{
					Thrusters.ForEach(Thruster => ThrusterOverrideIncrease(Thruster, float.Parse(_commandLine.Switch("increase", 0))));
				}
				catch
				{
					Echo("Unable to parse thruster override increase command's switch argument.\nArgument must be a decimal number between 0 and 100.\n");
				}
			}
			else if (_commandLine.Switch("decrease"))
			{
				try
				{
					Thrusters.ForEach(Thruster => ThrusterOverrideDecrease(Thruster, float.Parse(_commandLine.Switch("decrease", 0))));
				}
				catch
				{
					Echo("Unable to parse thruster override decrease command's switch argument.\nArgument must be a decimal number between 0 and 100.\n");
				}
			}
		}

		private void LightsCommand()
		{


			if (_commandLine.Switch("on")) { Lights.ForEach(EnableOn); }
			else if (_commandLine.Switch("off")) { Lights.ForEach(EnableOff); }

			if(_commandLine.Switch("setupnavlights"))
			{
				Lights.ForEach(SetupNavLight);
			}
		}
		#endregion

		#region Block properties

		private List<IMyShipDrill> Drills
		{
			get
			{
				List<IMyShipDrill> drills = new List<IMyShipDrill>();
				if (_commandLine.Switch((GroupSwitch)))
				{
					IMyBlockGroup group = GridTerminalSystem.GetBlockGroupWithName(_commandLine.Switch(GroupSwitch,0)) ?? null;

					if (group == null) { Error("Unable to parse group name for the -group switch. Syntax: -group [GroupName]. Does the group exist?"); }
					else
					{
						group.GetBlocksOfType(drills, Drill => Drill.IsSameConstructAs(Me));
					}
				}
				else
				{
					GridTerminalSystem.GetBlocksOfType(drills, Drill => Drill.IsSameConstructAs(Me));
				}
				return drills;
			}
		}

		private List<IMyShipWelder> Welders
		{
			get
			{
				List<IMyShipWelder> welders = new List<IMyShipWelder>();
				if (_commandLine.Switch((GroupSwitch)))
				{
					IMyBlockGroup group = GridTerminalSystem.GetBlockGroupWithName(_commandLine.Switch(GroupSwitch, 0)) ?? null;

					if (group == null) { Error("Unable to parse group name for the -group switch. Syntax: -group [GroupName]. Does the group exist?"); }
					else
					{
						group.GetBlocksOfType(welders, Welder => Welder.IsSameConstructAs(Me));
					}
				}
				else
				{
					GridTerminalSystem.GetBlocksOfType(welders, Welder => Welder.IsSameConstructAs(Me));
				}
				return welders;
			}
		}

		private List<IMyShipGrinder> Grinders
		{
			get
			{
				List<IMyShipGrinder> grinders = new List<IMyShipGrinder>();
				if (_commandLine.Switch((GroupSwitch)))
				{
					IMyBlockGroup group = GridTerminalSystem.GetBlockGroupWithName(_commandLine.Switch(GroupSwitch, 0)) ?? null;

					if (group == null) { Error("Unable to parse group name for the -group switch. Syntax: -group [GroupName]. Does the group exist?"); }
					else
					{
						group.GetBlocksOfType(grinders, Grinder => Grinder.IsSameConstructAs(Me));
					}
				}
				else
				{
					GridTerminalSystem.GetBlocksOfType(grinders, Grinder => Grinder.IsSameConstructAs(Me));
				}
				return grinders;
			}
		}

		private List<IMyShipConnector> Connectors
		{
			get
			{
				List<IMyShipConnector> connectors = new List<IMyShipConnector>();
				if (_commandLine.Switch((GroupSwitch)))
				{
					IMyBlockGroup group = GridTerminalSystem.GetBlockGroupWithName(_commandLine.Switch(GroupSwitch, 0)) ?? null;

					if (group == null) { Error("Unable to parse group name for the -group switch. Syntax: -group [GroupName]. Does the group exist?"); }
					else
					{
						group.GetBlocksOfType(connectors, Connector => Connector.IsSameConstructAs(Me));
					}
				}
				else
				{
					GridTerminalSystem.GetBlocksOfType(connectors, Connector => Connector.IsSameConstructAs(Me));
				}
				return connectors;
			}
		}

		private List<IMyBatteryBlock> Batteries
		{
			get
			{
				List<IMyBatteryBlock> batteries = new List<IMyBatteryBlock>();
				if (_commandLine.Switch((GroupSwitch)))
				{
					IMyBlockGroup group = GridTerminalSystem.GetBlockGroupWithName(_commandLine.Switch(GroupSwitch, 0)) ?? null;

					if (group == null) { Error("Unable to parse group name for the -group switch. Syntax: -group [GroupName]. Does the group exist?"); }
					else
					{
						group.GetBlocksOfType(batteries, Battery => Battery.IsSameConstructAs(Me));
					}
				}
				else
				{
					GridTerminalSystem.GetBlocksOfType(batteries, Battery => Battery.IsSameConstructAs(Me));
				}
				return batteries;
			}
		}

		private List<IMyThrust> Thrusters
		{
			get
			{
				List<IMyThrust> allThrusters = new List<IMyThrust>();
				List<IMyThrust> selection = new List<IMyThrust>();

				GridTerminalSystem.GetBlocksOfType(allThrusters, Thruster => Thruster.IsSameConstructAs(Me));

				if (_commandLine.Switch("backward"))
				{
					selection.AddRange(allThrusters.FindAll(Thruster => Thruster.GridThrustDirection == Vector3I.Backward));
				}
				if (_commandLine.Switch("forward"))
				{
					selection.AddRange(allThrusters.FindAll(Thruster => Thruster.GridThrustDirection == Vector3I.Forward));
				}
				if (_commandLine.Switch("up"))
				{
					selection.AddRange(allThrusters.FindAll(Thruster => Thruster.GridThrustDirection == Vector3I.Up));
				}
				if (_commandLine.Switch("down"))
				{
					selection.AddRange(allThrusters.FindAll(Thruster => Thruster.GridThrustDirection == Vector3I.Down));
				}
				if (_commandLine.Switch("left"))
				{
					selection.AddRange(allThrusters.FindAll(Thruster => Thruster.GridThrustDirection == Vector3I.Left));
				}
				if (_commandLine.Switch("right"))
				{
					selection.AddRange(allThrusters.FindAll(Thruster => Thruster.GridThrustDirection == Vector3I.Right));
				}

				return selection.Count > 0 ? selection : allThrusters;
			}
		}

		private List<IMyGyro> Gyroscopes
		{
			get
			{
				List<IMyGyro> gyroscopes = new List<IMyGyro>();
				if (_commandLine.Switch((GroupSwitch)))
				{
					IMyBlockGroup group = GridTerminalSystem.GetBlockGroupWithName(_commandLine.Switch(GroupSwitch, 0)) ?? null;

					if (group == null) { Error("Unable to parse group name for the -group switch. Syntax: -group [GroupName]. Does the group exist?"); }
					else
					{
						group.GetBlocksOfType(gyroscopes, Gyroscope => Gyroscope.IsSameConstructAs(Me));
					}
				}
				else
				{
					GridTerminalSystem.GetBlocksOfType(gyroscopes, Gyroscope => Gyroscope.IsSameConstructAs(Me));
				}
				return gyroscopes;
			}
		}

		private bool isDocked
		{
			get
			{
				foreach (IMyShipConnector connector in Connectors)
				{
					if (connector.Status == MyShipConnectorStatus.Connected)
					{
						return true;
					}
				}
				return false;
			}
		}

		private List<IMyLightingBlock> Lights
		{
			get
			{
				List<IMyLightingBlock> lights = new List<IMyLightingBlock>();
				if (_commandLine.Switch((GroupSwitch)))
				{
					IMyBlockGroup group = GridTerminalSystem.GetBlockGroupWithName(_commandLine.Switch(GroupSwitch, 0)) ?? null;

					if (group == null) { Error("Unable to parse group name for the -group switch. Syntax: -group [GroupName]. Does the group exist?"); }
					else
					{
						group.GetBlocksOfType(lights, item => item.IsSameConstructAs(Me));
					}
				}
				else
				{
					GridTerminalSystem.GetBlocksOfType(lights, item => item.IsSameConstructAs(Me));
				}
				return lights;
			}
		}
		#endregion

		#region Block state delegates
		private void ToggleEnable(IMyFunctionalBlock b) { b.Enabled = !b.Enabled; }

		private void EnableOn(IMyFunctionalBlock b) { b.Enabled = true; }

		private void EnableOff(IMyFunctionalBlock b) { b.Enabled = false; }

		private void BatteryRecharge(IMyBatteryBlock b) { b.ChargeMode = ChargeMode.Recharge; }

		private void BatteryDischarge(IMyBatteryBlock b) { b.ChargeMode = ChargeMode.Discharge; }

		private void BatteryAuto(IMyBatteryBlock b) { b.ChargeMode = ChargeMode.Auto; }

		private void ConnectorConnect(IMyShipConnector c) { c.Connect(); }

		private void ConnectorDisconnect(IMyShipConnector c) { c.Disconnect(); }

		private void ConnectorSwitch(IMyShipConnector c) { c.ToggleConnect(); }

		private void ThrusterOverrideFull(IMyThrust t) { t.ThrustOverridePercentage = 1.0f; }

		private void ThrusterOverrideNone(IMyThrust t) { t.ThrustOverridePercentage = 0.0f; }

		private void ThrusterOverrideSet(IMyThrust t, float num) 
		{
			if (num >= 0f && num <= 100f) { t.ThrustOverridePercentage = num / 100; }
			else Echo("Set thrust must be between 0 and 100\n");
		}

		private void ThrusterOverrideIncrease(IMyThrust t, float num)
		{
			t.ThrustOverridePercentage = Math.Min(t.ThrustOverridePercentage + (num / 100), 1.0f);
		}

		private void ThrusterOverrideDecrease(IMyThrust t, float num)
		{
			t.ThrustOverridePercentage = Math.Max(t.ThrustOverridePercentage - (num / 100), 0.0f);
		}

		private void ThrusterOverrideToggle(IMyThrust t)
		{
			if (t.ThrustOverridePercentage > 0f)
			{
				t.ThrustOverridePercentage = 0f;
			}
			else
			{
				t.ThrustOverridePercentage = 1f;
			}
		}

		private void SetupNavLight(IMyLightingBlock l)
		{
			string name = l.CustomName.ToLower();
			if (name.Contains("nav"))
			{
				if (name.Contains("left"))
				{
					l.Color = Color.Red;
				}
				else if (name.Contains("right"))
				{
					l.Color = Color.Green;
				}
				else if (name.Contains("back"))
				{
					l.Color = Color.White;
				}
				l.Intensity = 10.0f;
				l.Radius = 1.0f;
				l.Enabled = true;
			}

			if (name.Contains("strobe"))
			{
				l.Color = (name.Contains("wing")) ? Color.White : Color.Red;
				l.Intensity = 10.0f;
				l.Radius = 1.0f;
				l.BlinkIntervalSeconds = 1.0f;
				l.BlinkLength = 0.05f;
			}
		}
		#endregion
	}
}